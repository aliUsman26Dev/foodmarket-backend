<?php

namespace App\Http\Controllers\API;

use App\Helpers\ResponseFormatter;
use App\Http\Controllers\Controller;
use App\Models\User;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    public function Login(Request $request)
    {
        try {
            //untuk validasi input
            $request->validate(['email' => 'email|required', 'password' => 'requared']);
            //cek credentials(login)
            $credentials = request(['email', 'password']);
            if (!Auth::attempt($credentials)) {
                return ResponseFormatter::error(['message' => 'Unauthorised'], 'Authentication Failed', 500);
            }

            //jika hash tidak sesuai maka beri error
            $user = User::where('email', $request->email)->first();
            if (!Hash::check($request->password, $user->password, [])) {
                throw new  \Exception('Invalid Credentials');
            }
            //jika berhasil maka akan login
            $tokenResult = $user->createToken('authToken')->plainTextToken;
            return ResponseFormatter::success(['access_token' => $tokenResult, 'token_type' => 'Bearer', 'user' => $user], 'Authenticated');
        } catch (Exception $error) {
            return ResponseFormatter::error(['message' => 'Something Whent Wrong', 'error' => $error], 'Authenticated Failed', 500);
        }
    }
}
